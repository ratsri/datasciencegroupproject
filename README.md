# DataScienceGroupProject


## Detection of facial expressions using Neural Networks
This repository consists of all the necesseary files for the practical group project in 'Advanced Mathematics for Data Science' taught at the University of Zurich by Prof. Nikeghbali. The group members are Otto Buck, Keerthiga Rajakumar and Rathes Sriram.

## Description

In this project, we utilize a neural network to recognize facial expressions in images.

## Usage

The file main.py contains the entire code to read in the csv.file, train the model and test it. The report of the project is saved as DataScience_Report_Practical_Project_Keerthiga_Otto_Rathes.pdf

## References

https://www.kaggle.com/code/kanncaa1/convolutional-neural-network-cnn-tutorial

https://www.youtube.com/watch?v=Ixl3nykKG9M

https://www.kaggle.com/datasets/noamsegal/affectnet-training-data


## Project status
Submitted