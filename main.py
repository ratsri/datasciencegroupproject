"""
Created on 2024-06-27 09:59:48

@author: Rathes Sriram
Description: 

Implementation of the Neural Network to detect the emotion expressed in pictures. 

Project for the course 'Advanced Mathematics in Data Science' UZH FS24

Project members: Keerthiga, Otto and Rathes
"""

# %% 1) Import all necessary libraries

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D, BatchNormalization, GlobalAveragePooling2D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ReduceLROnPlateau

# %% 2.1) Loading the training data set

# Read in the training_data_set, i.e., 'training_data.csv'
train = pd.read_csv('/Users/ratsri/datasciencegroupproject/training_data.csv')
train.head()
print(train.shape)
# %% 2.2) Loading the testing data set

# Read in the testing_data_set, i.e., 'test_data.csv'
test = pd.read_csv('/Users/ratsri/datasciencegroupproject/test_data.csv')
print(test.shape)
test.head()

# %% 2.3.1) Drop the column 'Usage' in both data frames

train.drop(train.columns[1], axis=1, inplace=True)
test.drop(test.columns[1], axis=1, inplace=True)

# %% 2.3.2) Display the first five rows of train and test dataframes

train.head()
test.head()

# %% 2.3.3) Define Y_train as the column with all labels and drop it in train

Y_train = train[train.columns[0]]
X_train = train.drop(labels=train.columns[0], axis=1)
Y_test = test[test.columns[0]]
test = test.drop(labels=test.columns[0], axis=1)
print('Shape Y_train', Y_train.shape)
print(Y_train)

# %% 2.4) Display the first 5 rows of train df
X_train.head()

# %% 2.5) Display the first 5 rows of test df
test.head()

# %% 3) Visualize number of digit classes
plt.figure(figsize=(15,7))
g = sns.countplot(x=Y_train, palette='icefire')
plt.title("Number of digit classes")
Y_train.value_counts()

# %% 4.1) Plot Example 1 
img = X_train.iloc[0].values
print(img)
img = img.reshape((48,48))

plt.imshow(img, cmap='gray')
plt.title(train.iloc[0, 0])  # Assuming train contains the labels
plt.axis("off")
plt.show()

# %% 4.2) Plot Example 2
img = X_train.iloc[3].values
img = img.reshape((48,48))

plt.imshow(img, cmap='gray')
plt.title(train.iloc[0, 0])  # Assuming train contains the labels
plt.axis("off")
plt.show()

# %% 5) Normalize the data and then reshape it

X_train = X_train / 255.0
test = test / 255.0
print("x_train shape: ",X_train.shape)
print("test shape: ",test.shape)

X_train = X_train.values.reshape(-1,48,48,1)
test = test.values.reshape(-1,48,48,1)
print("x_train shape: ",X_train.shape)
print("test shape: ",test.shape)

# %% 6) Encode the labels

Y_train = to_categorical(Y_train, num_classes=7)
print(Y_train.shape)
print(Y_train)

# %% 7) Split the Training set in training set and validation set

X_train, X_val, Y_train, Y_val = train_test_split(X_train, Y_train, test_size = 0.05, random_state=2)
print("x_train shape",X_train.shape)
print("x_val shape",X_val.shape)
print("y_train shape",Y_train.shape)
print("y_val shape",Y_val.shape)

# %% 8) Create the model

# Model Definition
model = Sequential()
model.add(Conv2D(filters=8, kernel_size=(5, 5), padding='Same', activation='relu', input_shape=(48, 48, 1)))
model.add(BatchNormalization())
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='Same', activation='relu'))
model.add(BatchNormalization())
model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='Same', activation='relu'))
model.add(BatchNormalization())
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(GlobalAveragePooling2D())
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(7, activation='softmax'))

# Compile the model
model.compile(optimizer=Adam(), 
              loss='categorical_crossentropy', 
              metrics=['accuracy'])

# Print model summary
model.summary()

# %% 9) Change the optimizer

optimizer = Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999)

# %% 10) Compile the Model

model.compile(optimizer = optimizer , loss = "categorical_crossentropy", metrics=["accuracy"])

# %% 11) Epochs and Batch Size

epochs = 60
batch_size = 60

# %% 12) Data Augmentation

datagen = ImageDataGenerator(
    featurewise_center=False,
    samplewise_center=False,
    featurewise_std_normalization=False,
    samplewise_std_normalization=False,
    zca_whitening=False,
    rotation_range=10,  # Increased rotation range
    zoom_range=0.2,     # Increased zoom range
    width_shift_range=0.2,  # Increased width shift range
    height_shift_range=0.2, # Increased height shift range
    horizontal_flip=True,   # Enabled horizontal flip
    vertical_flip=False
)

datagen.fit(X_train)

# %% 13) Learning Rate Scheduler

reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5, min_lr=0.00001)

# %% 14) Fit the model
history = model.fit(datagen.flow(X_train, Y_train, batch_size=batch_size),
                    epochs=epochs, validation_data=(X_val, Y_val), 
                    steps_per_epoch=X_train.shape[0] // batch_size,
                    callbacks=[reduce_lr])

# %%
# %% 15) Preprocess the test data

# Ensure the test data has been normalized and reshaped (already done in previous steps)
print("test shape: ", test.shape)

# %% 16) Make predictions on the test data

# Predict class probabilities
test_predictions = model.predict(test)

# Convert probabilities to class labels
test_predicted_labels = np.argmax(test_predictions, axis=1)

# %% 17) Evaluate the performance

# Assuming you have the true labels for the test data in a similar way as Y_train
# Read in the true test labels
#Y_test = pd.read_csv('/Users/ratsri/datasciencegroupproject/test_labels.csv')  # Assuming the true labels are stored in a separate file
Y_test = to_categorical(Y_test, num_classes=7)

# Convert true labels to class labels
true_test_labels = np.argmax(Y_test, axis=1)

# Compute accuracy
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

test_accuracy = accuracy_score(true_test_labels, test_predicted_labels)
print("Test Accuracy: {:.2f}%".format(test_accuracy * 100))

# Print classification report
print("Classification Report:")
print(classification_report(true_test_labels, test_predicted_labels, target_names=["Class 0", "Class 1", "Class 2", "Class 3", "Class 4", "Class 5", "Class 6"]))

# Print confusion matrix
print("Confusion Matrix:")
print(confusion_matrix(true_test_labels, test_predicted_labels))

# %% 18) Evaluate the model on the test data

test_loss, test_accuracy = model.evaluate(test, Y_test, verbose=2)
print("Test Loss: {:.4f}".format(test_loss))
print("Test Accuracy: {:.2f}%".format(test_accuracy * 100))

# %% 19) Plot the loss function

# Extract loss and accuracy from the training history
train_loss = history.history['loss']
val_loss = history.history['val_loss']
epochs_range = range(1, epochs + 1)

plt.figure(figsize=(10, 5))

# Plot training and validation loss
plt.plot(epochs_range, train_loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')



plt.title('Loss over Epochs')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend(loc='upper right')
plt.show()

# %%
